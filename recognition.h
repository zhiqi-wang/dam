#ifndef RECOGNITION_H
#define RECOGNITION_H

#include <QDialog>
#include <QSqlTableModel>
#include <QStringListModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include<iostream>
#include<math.h>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <stack>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QLabel>

using namespace cv;
using namespace std;

namespace Ui {
class Recognition;
}

class Recognition : public QDialog
{
    Q_OBJECT

public:
    explicit Recognition(QWidget *parent = nullptr);
    ~Recognition();
    QStringListModel *Model;
    QStandardItemModel *ItemModel;
    static QStringList srcDirPathListS;
    double average2(const vector<double>& arr, int first, int length);
    void Showlargest(Mat srcMat, Mat binaryMat, Mat& resultMat, int X);
    void Showright(Mat& resultMat, int X1, int X2);
    void Showleft(Mat& resultMat, int X1, int X2);
    void Duiqi(Mat& resultMat, int X2, int W);
    void getSizeContours(vector<vector<Point>>& contours);
    void getSizeContours2(vector<vector<Point>>& contours);
    int split(string str,string get[],char c);//字符串分割
    void minRect(Mat &inImgM, Mat &outImgM);
    QImage ImageCenter(QImage qimage,QLabel *qLabel);//调整图片比例
    int index =0;//图片index
    QString img_path_1;
    QString img_path_2;
    QString img_path_3;
    QString img_path_4;
    QString img_path_5;
    int flag = 0;

private slots:

    void on_open_files_clicked();

    void on_showresult_clicked();

    void on_next_button_clicked();

    void on_last_button_clicked();

    void on_label_other_1_button_clicked();

    void on_label_other_2_button_clicked();

    void on_label_other_3_button_clicked();

    void on_label_other_4_button_clicked();

    void on_label_other_5_button_clicked();

//    void on_collapse_identification_clicked();

//    void on_showresult_collapse_clicked();

    void on_infrared_identification_clicked();

    void on_showresult_infrared_clicked();



private:
    Ui::Recognition *ui;
    QSqlTableModel *model;

};

#endif // RECOGNITION_H
