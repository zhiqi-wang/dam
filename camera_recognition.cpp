#include "camera_recognition.h"
#include "ui_camera_recognition.h"
#include <QMessageBox>
#include <QSqlError>
#include <QSqlDatabase>
#include <QDebug>
#include <QDirIterator>
#include <QDir>
#include <QFileInfo>
#include <QFileDialog>
#include <iostream>
#include <string>
#include <QProgressDialog>
#include <list>
#include <vector>
#include <map>
#include <stack>
#include <opencv2/imgproc/types_c.h>

using namespace std;
using namespace cv;

camera_recognition::camera_recognition(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::camera_recognition)
{
    ui->setupUi(this);
}

camera_recognition::~camera_recognition()
{
    delete ui;
}


//图片居中显示,图片大小与label大小相适应
QImage camera_recognition::ImageCenter(QImage qimage, QLabel *qLabel)
{
    QImage image;
    QSize imageSize = qimage.size();
    QSize labelSize = qLabel->size();

    double dWidthRatio = 1.0*imageSize.width() / labelSize.width();
    double dHeightRatio = 1.0*imageSize.height() / labelSize.height();
            if (dWidthRatio>dHeightRatio)
            {
                image = qimage.scaledToWidth(labelSize.width());
            }
            else
            {
                image = qimage.scaledToHeight(labelSize.height());
            }
            return image;
}

void camera_recognition::getSizeContours(vector<vector<Point> > &contours)
{
    int cmin = 100;   // 最小轮廓长度
    int cmax = 1000;   // 最大轮廓长度
    vector<vector<Point>>::const_iterator itc = contours.begin();
    while (itc != contours.end())
    {
        if ((itc->size()) < cmin || (itc->size()) > cmax)
        {
            itc = contours.erase(itc);
        }
        else ++itc;
    }
}

//查看白天塌陷
void camera_recognition::on_showresult_collapse_clicked()
{
    flag = 1;
    QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
    QStringList nameFilters;
    nameFilters << "*.jpg" << "*.png";
    QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
    index = 0;
    QString img_path = img_files->at(index).filePath();
    if(img_path.isEmpty())
    {
         return;
    }
    else
    {
        QImage* img=new QImage;
        if(! (img->load(img_path) ) ) //加载图像路径
        {
            QMessageBox::information(this,
                                     tr("打开图像失败"),
                                     tr("打开图像失败!"));
            delete img;
            return;
        }
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
        //label_other_1
        QString img_path_1 = img_files->at(index).filePath();
        QImage image1(img_path_1);
        QImage Image1 = ImageCenter(image1,ui->label_other_1);
        ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
        ui->label_other_1->setAlignment(Qt::AlignCenter);
        //label_other_2
        QString img_path_2 = img_files->at(index+1).filePath();
        QImage image2(img_path_2);
        QImage Image2 = ImageCenter(image2,ui->label_other_2);
        ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
        ui->label_other_2->setAlignment(Qt::AlignCenter);
        //label_other_3
        QString img_path_3 = img_files->at(index+2).filePath();
        QImage image3(img_path_3);
        QImage Image3 = ImageCenter(image3,ui->label_other_3);
        ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
        ui->label_other_3->setAlignment(Qt::AlignCenter);
        //label_other_4
        QString img_path_4 = img_files->at(index+3).filePath();
        QImage image4(img_path_4);
        QImage Image4 = ImageCenter(image4,ui->label_other_4);
        ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
        ui->label_other_4->setAlignment(Qt::AlignCenter);
        //label_other_5
        QString img_path_5 = img_files->at(index+4).filePath();
        QImage image5(img_path_5);
        QImage Image5 = ImageCenter(image5,ui->label_other_5);
        ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
        ui->label_other_5->setAlignment(Qt::AlignCenter);
        QString str = "当前图片编号:";
        QString str_index = QString::number(index+1);
        str.append(str_index);
        QString str_count = QString::number(img_files->count());
        QString str1 = "    图片总数:";
        str.append(str1);
        str.append(str_count);
        ui->label->setText(str);
        QString str2 = "无塌陷";
        //ui->label_result->setText(str2);
    }
}

//上一张图片
void camera_recognition::on_last_button_clicked()
{
    if(flag == 1)
    {
        if(index >= 1)
        {
            index=(index-1);
            QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
            QStringList nameFilters;
            nameFilters << "*.jpg" << "*.png";
            QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                ui->label->setText(str);
                QString str2 = "无塌陷";
                //ui->label_result->setText(str2);
            }
        }else
        {
            index = 0;
        }
    }
    if(flag == 2)
    {
        if(index >= 1)
        {
            index=(index-1);
            QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
            QStringList nameFilters;
            nameFilters << "*.jpg" << "*.png";
            QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                ui->label->setText(str);
                QString str2 = "无塌陷";
                //ui->label_result->setText(str2);
            }
        }else
        {
            index = 0;
        }
    }
}

//下一张图片
void camera_recognition::on_next_button_clicked()
{
    if(flag==1)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        if(index<img_files->count()-5)
        {
            index=qAbs(index+1);
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                ui->label->setText(str);
                QString str2 = "无塌陷";
                //ui->label_result->setText(str2);
            }
        }else
        {
            index = qAbs(img_files->count()-5);
        }
    }
    if(flag == 2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        if(index<img_files->count()-5)
        {
            index=qAbs(index+1);
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                ui->label->setText(str);
                QString str2 = "无塌陷";
                //ui->label_result->setText(str2);
            }
        }else
        {
            index = qAbs(img_files->count()-5);
        }
    }
}

void camera_recognition::on_label_other_1_button_clicked()
{
    if(flag==1)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void camera_recognition::on_label_other_1_button_2_clicked()
{
    if(flag==1)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+1).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+1).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void camera_recognition::on_label_other_1_button_3_clicked()
{
    if(flag==1)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+2).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+2).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void camera_recognition::on_label_other_1_button_4_clicked()
{
    if(flag==1)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+3).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+3).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void camera_recognition::on_label_other_1_button5_clicked()
{
    if(flag==1)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/day_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+4).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
    }
    if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+4).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
    }
}


//白天塌陷识别
void camera_recognition::on_collapse_identification_clicked()
{
    //创建输出结果文件夹
    QString dir_str = "C:/Users/a1124/Desktop/Desktop/camera_result/day_result";
    QDir file_dir;
    if (!file_dir.exists(dir_str))
    {
        bool res = file_dir.mkpath(dir_str);
        qDebug() << "新建目录是否成功" << res;
    }
    qDebug() << "文件夹已存在";
    //选择待识别的图片
    QString filename = QFileDialog::getExistingDirectory();
    QDir *dir=new QDir(filename);
    QStringList filter;
    filter << "*.jpg" << "*.png";
    QList<QFileInfo> *fileInfo=new QList<QFileInfo>(dir->entryInfoList(filter));
    QProgressDialog process(this);
    QString str = "图片总数:";
    QString filecount = QString::number(fileInfo->count());
    str.append(filecount);
    process.setWindowTitle(str);
    process.setFixedWidth(300);
    process.setLabelText(tr("正在识别..."));
    process.setRange(0,fileInfo->count());
    process.setModal(true);
    process.setCancelButtonText(tr("取消"));
    //循环识别
    for(int i = 0;i<fileInfo->count(); i++)
    {
        Mat imgsrc;
        Mat original_image = cv::imread("C:\\Users\\a1124\\Desktop\\Desktop\\test3\\G64563728_1_20220417T173100Z.jpg");
        imgsrc = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);

        if((original_image.rows != imgsrc.rows) || (original_image.cols != imgsrc.cols))
        {
            if (original_image.rows > imgsrc.rows)
            {
                cv::resize(original_image, original_image, imgsrc.size(), 0, 0, INTER_LINEAR);
            }
            else if (original_image.rows < imgsrc.rows)
            {
                cv::resize(imgsrc, imgsrc, original_image.size(), 0, 0, INTER_LINEAR);
            }
        }
        Mat original_image_gary, imgsrc_gray;
        if (original_image.channels() != 1)
        {
            cvtColor(original_image, original_image_gary, COLOR_BGR2GRAY);
        }
        if (imgsrc.channels() != 1)
        {
            cvtColor(imgsrc, imgsrc_gray, COLOR_BGR2GRAY);
        }
        Mat frameDifference, absFrameDifferece;
        Mat previousGrayFrame = imgsrc_gray.clone();
        //图1减图2
        subtract(original_image_gary, imgsrc_gray, frameDifference, Mat(), CV_16SC1);

        //取绝对值
        absFrameDifferece = abs(frameDifference);

        //位深的改变
        absFrameDifferece.convertTo(absFrameDifferece, CV_8UC1, 1, 0);
        Mat segmentation;

        //阈值处理（这一步很关键，要调好二值化的值）
        threshold(absFrameDifferece, segmentation,100, 255, THRESH_BINARY);

        //中值滤波
        medianBlur(segmentation, segmentation, 3);

        //形态学处理(开闭运算)
        //形态学处理用到的算子
        Mat morphologyKernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
        morphologyEx(segmentation, segmentation, MORPH_CLOSE, morphologyKernel, Point(-1, -1), 2, BORDER_REPLICATE);

        //找边界
        vector< vector<Point> > contours;
        vector<Vec4i> hierarchy;
        findContours(segmentation, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));//CV_RETR_TREE
        getSizeContours(contours);
        vector< vector<Point> > contours_poly(contours.size());
        vector<Rect> boundRect;
        boundRect.clear();
        for (int index = 0; index < contours.size(); index++)
        {
            approxPolyDP(Mat(contours[index]), contours_poly[index], 3, true);
            Rect rect = boundingRect(Mat(contours_poly[index]));
            rectangle(imgsrc, rect, Scalar(0, 0, 255), 2);
        }

        char a[300];
        sprintf_s(a, "C:/Users/a1124/Desktop/Desktop/camera_result/day_result/%d.JPG", i);
        process.setValue(i);
        if(process.wasCanceled())
            break;
        imwrite(a, imgsrc);
        QImage* img=new QImage;
        img->load(fileInfo->at(i).filePath());
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
    }
    QMessageBox::about(this,"提示","识别成功");
}


//夜晚塌陷识别
void camera_recognition::on_collapse_identification_night_clicked()
{
    //创建输出结果文件夹
    QString dir_str = "C:/Users/a1124/Desktop/Desktop/camera_result/night_result";
    QDir file_dir;
    if (!file_dir.exists(dir_str))
    {
        bool res = file_dir.mkpath(dir_str);
        qDebug() << "新建目录是否成功" << res;
    }
    qDebug() << "文件夹已存在";
    //选择待识别的图片
    QString filename = QFileDialog::getExistingDirectory();
    QDir *dir=new QDir(filename);
    QStringList filter;
    filter << "*.jpg" << "*.png";
    QList<QFileInfo> *fileInfo=new QList<QFileInfo>(dir->entryInfoList(filter));
    QProgressDialog process(this);
    QString str = "图片总数:";
    QString filecount = QString::number(fileInfo->count());
    str.append(filecount);
    process.setWindowTitle(str);
    process.setFixedWidth(300);
    process.setLabelText(tr("正在识别..."));
    process.setRange(0,fileInfo->count());
    process.setModal(true);
    process.setCancelButtonText(tr("取消"));
    //循环识别
    for(int i = 0;i<fileInfo->count(); i++)
    {
        Mat imgsrc;
        Mat original_image = cv::imread("C:\\Users\\a1124\\Desktop\\Desktop\\test2\\captrue\\G64563785_1_20220420T220934Z.jpg");
        imgsrc = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);

        if((original_image.rows != imgsrc.rows) || (original_image.cols != imgsrc.cols))
        {
            if (original_image.rows > imgsrc.rows)
            {
                cv::resize(original_image, original_image, imgsrc.size(), 0, 0, INTER_LINEAR);
            }
            else if (original_image.rows < imgsrc.rows)
            {
                cv::resize(imgsrc, imgsrc, original_image.size(), 0, 0, INTER_LINEAR);
            }
        }
        Mat original_image_gary, imgsrc_gray;
        if (original_image.channels() != 1)
        {
            cvtColor(original_image, original_image_gary, COLOR_BGR2GRAY);
        }
        if (imgsrc.channels() != 1)
        {
            cvtColor(imgsrc, imgsrc_gray, COLOR_BGR2GRAY);
        }
        Mat frameDifference, absFrameDifferece;
        Mat previousGrayFrame = imgsrc_gray.clone();
        //图1减图2
        subtract(original_image_gary, imgsrc_gray, frameDifference, Mat(), CV_16SC1);

        //取绝对值
        absFrameDifferece = abs(frameDifference);

        //位深的改变
        absFrameDifferece.convertTo(absFrameDifferece, CV_8UC1, 1, 0);
        Mat segmentation;

        //阈值处理（这一步很关键，要调好二值化的值）
        threshold(absFrameDifferece, segmentation,100, 255, THRESH_BINARY);

        //中值滤波
        medianBlur(segmentation, segmentation, 3);

        //形态学处理(开闭运算)
        //形态学处理用到的算子
        Mat morphologyKernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));
        morphologyEx(segmentation, segmentation, MORPH_CLOSE, morphologyKernel, Point(-1, -1), 2, BORDER_REPLICATE);

        //找边界
        vector< vector<Point> > contours;
        vector<Vec4i> hierarchy;
        findContours(segmentation, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));//CV_RETR_TREE
        getSizeContours(contours);
        vector< vector<Point> > contours_poly(contours.size());
        vector<Rect> boundRect;
        boundRect.clear();
        for (int index = 0; index < contours.size(); index++)
        {
            approxPolyDP(Mat(contours[index]), contours_poly[index], 3, true);
            Rect rect = boundingRect(Mat(contours_poly[index]));
            rectangle(imgsrc, rect, Scalar(0, 0, 255), 2);
        }

        char a[300];
        sprintf_s(a, "C:/Users/a1124/Desktop/Desktop/camera_result/night_result/%d.JPG", i);
        process.setValue(i);
        if(process.wasCanceled())
            break;
        imwrite(a, imgsrc);
        QImage* img=new QImage;
        img->load(fileInfo->at(i).filePath());
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
    }
    QMessageBox::about(this,"提示","识别成功");
}

//查看夜晚塌陷
void camera_recognition::on_showresult_collapse_night_clicked()
{
    flag = 2;
    QDir dir("C:/Users/a1124/Desktop/Desktop/camera_result/night_result");
    QStringList nameFilters;
    nameFilters << "*.jpg" << "*.png";
    QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
    index = 0;
    QString img_path = img_files->at(index).filePath();
    if(img_path.isEmpty())
    {
         return;
    }
    else
    {
        QImage* img=new QImage;
        if(! (img->load(img_path) ) ) //加载图像路径
        {
            QMessageBox::information(this,
                                     tr("打开图像失败"),
                                     tr("打开图像失败!"));
            delete img;
            return;
        }
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
        //label_other_1
        QString img_path_1 = img_files->at(index).filePath();
        QImage image1(img_path_1);
        QImage Image1 = ImageCenter(image1,ui->label_other_1);
        ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
        ui->label_other_1->setAlignment(Qt::AlignCenter);
        //label_other_2
        QString img_path_2 = img_files->at(index+1).filePath();
        QImage image2(img_path_2);
        QImage Image2 = ImageCenter(image2,ui->label_other_2);
        ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
        ui->label_other_2->setAlignment(Qt::AlignCenter);
        //label_other_3
        QString img_path_3 = img_files->at(index+2).filePath();
        QImage image3(img_path_3);
        QImage Image3 = ImageCenter(image3,ui->label_other_3);
        ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
        ui->label_other_3->setAlignment(Qt::AlignCenter);
        //label_other_4
        QString img_path_4 = img_files->at(index+3).filePath();
        QImage image4(img_path_4);
        QImage Image4 = ImageCenter(image4,ui->label_other_4);
        ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
        ui->label_other_4->setAlignment(Qt::AlignCenter);
        //label_other_5
        QString img_path_5 = img_files->at(index+4).filePath();
        QImage image5(img_path_5);
        QImage Image5 = ImageCenter(image5,ui->label_other_5);
        ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
        ui->label_other_5->setAlignment(Qt::AlignCenter);
        QString str = "当前图片编号:";
        QString str_index = QString::number(index+1);
        str.append(str_index);
        QString str_count = QString::number(img_files->count());
        QString str1 = "    图片总数:";
        str.append(str1);
        str.append(str_count);
        ui->label->setText(str);
        QString str2 = "无塌陷";
        //ui->label_result->setText(str2);
    }
}
