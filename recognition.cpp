#include "recognition.h"
#include "ui_recognition.h"
#include <string.h>
#include <stdio.h>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QDateTime>
#include <QDirIterator>
#include <QDir>
#include <QFileInfo>
#include <QFileDialog>
#include <iostream>
#include <string>
#include <QProgressDialog>
#include <list>
#include <vector>
#include <map>
#include <stack>
#include <opencv2/imgproc/types_c.h>

using namespace std;
using namespace cv;

#define CV_RGB(r,g,b) cvScalar((b),(g),(r))

Recognition::Recognition(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Recognition)
{
    ui->setupUi(this);
    setWindowFlags(windowFlags()&~Qt::WindowMaximizeButtonHint);    // 禁止最大化按钮
    setFixedSize(this->width(),this->height());                     // 禁止拖动窗口大小
}

Recognition::~Recognition()
{
    delete ui;
}



double Recognition::average2(const vector<double> &arr, int first, int length)
{
    double sum = 0;
        int k = 0;
        if (arr.size() - first < length)
        {
            for (int i = first; i < arr.size(); i++)
            {
                sum += arr[i];
                k = k + 1;
                if (k == length)
                {
                    break;
                }
            }
            return sum / (arr.size() - first);
        }
        else
        {
            for (int i = first; i < arr.size(); i++)
            {
                sum += arr[i];
                k = k + 1;
                if (k == length)
                {
                    break;
                }
            }
            return sum / length;
        }
}

void Recognition::Showlargest(Mat srcMat, Mat binaryMat, Mat &resultMat, int X)
{
    Mat labelMat;
        Mat statsMat;
        Mat centrMat;
        int nComp = cv::connectedComponentsWithStats(binaryMat, labelMat, statsMat, centrMat, X, CV_32S);
        int maxpix = 0;//最大连通域的像素点数最大连通域的序号
        int max = 0;//最大连通域的序号
        for (int i = 1; i < nComp; i++)
        {
            if (statsMat.at<int>(i, 4) > maxpix)
            {
                maxpix = statsMat.at<int>(i, 4);
                max = i;
            }
        }
        resultMat = cv::Mat::zeros(srcMat.size(), CV_8UC3);
        std::vector<cv::Vec3b> colors(nComp);
        colors[0] = cv::Vec3b(0, 0, 0);	//背景使用黑色
        colors[max] = cv::Vec3b(255, 255, 255);	//最大连通域使用白色
        //对所有像素按照连通域编号进行着色（只有黑白）
        for (int y = 0; y < srcMat.rows; y++)
        {
            for (int x = 0; x < srcMat.cols; x++)
            {
                int label = labelMat.at<int>(y, x);
                CV_Assert(0 <= label && label <= nComp);
                resultMat.at<cv::Vec3b>(y, x) = colors[label];
            }
        }
}

void Recognition::Showright(Mat &resultMat, int X1, int X2)
{
    int i, j;
        int cPointR, cPointG, cPointB;
        int a = 0;
        //int X = 200;//设置宽度像素
        for (i = 0; i < resultMat.rows; i++)
        {
            for (j = resultMat.cols - 1; j >= 0; j--)
            {
                cPointB = resultMat.at<Vec3b>(i, j)[0];
                cPointG = resultMat.at<Vec3b>(i, j)[1];
                cPointR = resultMat.at<Vec3b>(i, j)[2];
                if (cPointB == 255 && cPointG == 255 && cPointR == 255)
                {
                    a = j;
                    break;
                }
            }
            if (a > 0) {
                for (j = 0; j < resultMat.cols; j++)
                {
                    if (j > a + X1 && j <= a + X2)
                    {
                        resultMat.at<Vec3b>(i, j)[0] = 255;
                        resultMat.at<Vec3b>(i, j)[1] = 255;
                        resultMat.at<Vec3b>(i, j)[2] = 255;
                    }
                    else
                    {
                        resultMat.at<Vec3b>(i, j)[0] = 0;
                        resultMat.at<Vec3b>(i, j)[1] = 0;
                        resultMat.at<Vec3b>(i, j)[2] = 0;
                    }
                }
                a = 0;
            }
        }
}

void Recognition::Showleft(Mat &resultMat, int X1, int X2)
{
    int i, j;
        int cPointR, cPointG, cPointB;
        int a = 0;
        //int X = 200;//设置宽度像素
        for (i = 0; i < resultMat.rows; i++)
        {
            for (j = resultMat.cols - 1; j >= 0; j--)
            {
                cPointB = resultMat.at<Vec3b>(i, j)[0];
                cPointG = resultMat.at<Vec3b>(i, j)[1];
                cPointR = resultMat.at<Vec3b>(i, j)[2];
                if (cPointB == 255 && cPointG == 255 && cPointR == 255)
                {
                    a = j;
                    break;
                }
            }

                for (j = 0; j < resultMat.cols; j++)
                {
                    if (j > a - X2 && j <= a - X1)
                    {
                        resultMat.at<Vec3b>(i, j)[0] = 255;
                        resultMat.at<Vec3b>(i, j)[1] = 255;
                        resultMat.at<Vec3b>(i, j)[2] = 255;
                    }
                    else
                    {
                        resultMat.at<Vec3b>(i, j)[0] = 0;
                        resultMat.at<Vec3b>(i, j)[1] = 0;
                        resultMat.at<Vec3b>(i, j)[2] = 0;
                    }
                }
        }
}

void Recognition::Duiqi(Mat &resultMat, int X2, int W)
{
    int i, j, k;
        k = 0;
        int a = 0;
        int cPointR, cPointG, cPointB;
        for (i = 0; i < resultMat.rows; i++)
        {
            for (j = resultMat.cols - 1; j >= 0; j--)
            {
                cPointB = resultMat.at<Vec3b>(i, j)[0];
                cPointG = resultMat.at<Vec3b>(i, j)[1];
                cPointR = resultMat.at<Vec3b>(i, j)[2];
                if (cPointB == 255 && cPointG == 255 && cPointR == 255)
                {
                    k = k + 1;
                    break;
                }
            }
        }
        vector<double> arr(k);
        k = 0;
        for (i = 0; i < resultMat.rows; i++)
        {
            for (j = 0; j < resultMat.cols; j++)
            {
                cPointB = resultMat.at<Vec3b>(i, j)[0];
                cPointG = resultMat.at<Vec3b>(i, j)[1];
                cPointR = resultMat.at<Vec3b>(i, j)[2];
                if (cPointB == 255 && cPointG == 255 && cPointR == 255)
                {
                    arr[k] = j;
                    k = k + 1;
                    break;
                }
            }
        }
        int s = -1;
        for (i = 0; i < resultMat.rows; i++)
        {
            for (j = resultMat.cols - 1; j >= 0; j--)
            {
                cPointB = resultMat.at<Vec3b>(i, j)[0];
                cPointG = resultMat.at<Vec3b>(i, j)[1];
                cPointR = resultMat.at<Vec3b>(i, j)[2];
                if (cPointB == 255 && cPointG == 255 && cPointR == 255)
                {
                    a = j;
                    s = s + 1;
                    break;
                }
            }
            if (a > 0)
            {
                int Y = average2(arr, s, W);
                for (j = 0; j < resultMat.cols; j++)
                {
                    if (j > Y && j <= Y + X2)
                    {
                        resultMat.at<Vec3b>(i, j)[0] = 255;
                        resultMat.at<Vec3b>(i, j)[1] = 255;
                        resultMat.at<Vec3b>(i, j)[2] = 255;
                    }
                    else
                    {
                        resultMat.at<Vec3b>(i, j)[0] = 0;
                        resultMat.at<Vec3b>(i, j)[1] = 0;
                        resultMat.at<Vec3b>(i, j)[2] = 0;
                    }
                }
                a = 0;
            }
        }
}

//红外图像大小阈值
void Recognition::getSizeContours(vector<vector<Point> > &contours)
{
    int cmin = 500;   // 最小轮廓长度
    int cmax = 1000;   // 最大轮廓长度
    vector<vector<Point>>::const_iterator itc = contours.begin();
    while (itc != contours.end())
    {
        if ((itc->size()) < cmin || (itc->size()) > cmax)
        {
            itc = contours.erase(itc);
        }
        else ++itc;
    }
}

//可见光图像大小阈值
void Recognition::getSizeContours2(vector<vector<Point> > &contours)
{
    int cmin = 100;   // 最小轮廓长度
    int cmax = 500;   // 最大轮廓长度
    vector<vector<Point>>::const_iterator itc = contours.begin();
    while (itc != contours.end())
    {
        if ((itc->size()) < cmin || (itc->size()) > cmax)
        {
            itc = contours.erase(itc);
        }
        else ++itc;
    }
}


int Recognition::split(string str, string get[], char c)
{
    int pos=0;//指向字符分割开始的的位置
        int l_pos=0;//指向分割结束的位置
        int count=0;//计数，保留分割字符串的数量

        for(int i=0;i<str.length();i++){
            if(i==str.length()-1&&str[i]!=c){//若读到最后一个字符，将最后一个分割后的字符串填入get[]
                l_pos=str.length();
                get[count]=string(str,pos,l_pos-pos);
                pos=l_pos+1;
                ++count;
            }
            else if(str[i]==c){//分割
                l_pos=i;
                get[count]=string(str,pos,l_pos-pos);
                pos=l_pos+1;
                ++count;
            }
        }
        return count;
}

//提取渗水点的外接矩形，效果不错，但3层循环放在界面函数类跑不动，改进的话可以尝试采用多线程，把这个函数放在子线程内执行
void Recognition::minRect(Mat &inImgM, Mat &outImgM)
{
    Mat src = inImgM.clone();
        Mat src_gray, src_canny, dst;

        //图像预处理
        cvtColor(src, src_gray, COLOR_RGB2GRAY);
        GaussianBlur(src_gray, dst, Size(5, 5), 10, 10);
        Canny(dst, src_canny, 250, 100, 3);
        //imshow("canny检测", src_canny);
        Mat element = getStructuringElement(MORPH_RECT, Size(4, 2)); //定义结构元素
        dilate(src_canny, src_canny, element); //膨胀
        //imshow("膨胀", src_canny);

        vector<vector<Point>> contours;
        vector<Vec4i> hierarcy;
        findContours(src_canny, contours, hierarcy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE); //寻找轮廓
        vector<Rect> boundRect(contours.size());
        vector<RotatedRect>box(contours.size());
        Point2f rect[4];

        for (int i = 0; i < contours.size(); i++)
        {
            box[i] = minAreaRect(Mat(contours[i]));  //最小外接矩形(带角度)
            boundRect[i] = boundingRect(Mat(contours[i]));   //最小外接矩形(水平)
            box[i].points(rect);//把最小外接矩形四个端点复制给rect数组

            Mat ROI_t;
            int x_t0 = 0, y_t0 = 0, w_t0 = 0, h_t0 = 0;

            //取带角度的目标框
            x_t0 = box[i].center.x - (box[i].size.width / 2);
            y_t0 = box[i].center.y - (box[i].size.height / 2);
            w_t0 = box[i].size.width;
            h_t0 = box[i].size.height;

            //取水平目标框
            //x_t0 = boundRect[i].x;
            //y_t0 = boundRect[i].y;
            //w_t0 = boundRect[i].width;
            //h_t0 = boundRect[i].height;
            //ROI_t = src(Rect(x_t0, y_t0, w_t0, h_t0));

            //画目标框
            //水平框
            //rectangle(src, Point(boundRect[i].x, boundRect[i].y), Point(boundRect[i].x + boundRect[i].width, boundRect[i].y + boundRect[i].height), Scalar(0, 255, 0), 2, 8);
            //带角度框
            for (int j = 0; j<4; j++) {
                line(src, rect[j], rect[(j + 1) % 4], Scalar(0, 0, 255), 2, 8);
            }

            //画中心点
            //circle(src, Point(box[i].center.x, box[i].center.y), 3, Scalar(0, 0, 255), -1, 8);
            //imshow("Img1", src);
        }
        outImgM = src.clone();
}

//图片居中显示,图片大小与label大小相适应
QImage Recognition::ImageCenter(QImage qimage, QLabel *qLabel)
{
    QImage image;
    QSize imageSize = qimage.size();
    QSize labelSize = qLabel->size();

    double dWidthRatio = 1.0*imageSize.width() / labelSize.width();
    double dHeightRatio = 1.0*imageSize.height() / labelSize.height();
            if (dWidthRatio>dHeightRatio)
            {
                image = qimage.scaledToWidth(labelSize.width());
            }
            else
            {
                image = qimage.scaledToHeight(labelSize.height());
            }
            return image;
}

//连接mysql数据库
//void Recognition::on_connect_db_clicked()
//{
//    // 添加MySql数据库
//    QSqlDatabase db=QSqlDatabase::addDatabase("QMYSQL");
//    // 连接数据库
//    db.setHostName("localhost");             // 数据库服务器IP，数据库支持远程连接，这里localhost是本地数据库
//    db.setUserName("root");                  // 设置数据库用户名
//    db.setPassword("Aa1124045803");          // 设置密码
//    db.setDatabaseName("fx");              // 设置数据库名，使用哪个数据库，数据库要本来就存在
//    db.open();

//    if(db.open())
//    {
//        qDebug()<<"连接数据库成功";
//        QMessageBox::about(this,"提示","数据库连接成功!");
//    }
//    else
//    {
//        qDebug()<<db.lastError().text();
//        QMessageBox::warning(this,"错误","error open database because : "+db.lastError().text());
//    }
//}


//可见光图像识别
void Recognition::on_open_files_clicked()
{
    //创建输出结果文件夹
    QString dir_str = "./result/light_result";
    QDir file_dir;
    if (!file_dir.exists(dir_str))
    {
        bool res = file_dir.mkpath(dir_str);
        qDebug() << "新建目录是否成功" << res;
    }
    qDebug() << "文件夹已存在";
    //选择待识别的图片
    QString filename = QFileDialog::getExistingDirectory();
    QDir *dir=new QDir(filename);
    QStringList filter;
    filter << "*.jpg" << "*.png";
    QList<QFileInfo> *fileInfo=new QList<QFileInfo>(dir->entryInfoList(filter));
    QProgressDialog process(this);
    QString str = "图片总数:";
    QString filecount = QString::number(fileInfo->count());
    str.append(filecount);
    process.setWindowTitle(str);
    process.setFixedWidth(300);
    process.setLabelText(tr("正在识别..."));
    process.setRange(0,fileInfo->count());
    process.setModal(true);
    process.setCancelButtonText(tr("取消"));

    //连接数据库，将识别结果写入数据库
    QSqlDatabase dbconn = QSqlDatabase::addDatabase("QSQLITE");
    dbconn.setDatabaseName("./datebase/fx.db");  //数据库文件
    if(!dbconn.open())
    {
        qDebug()<<dbconn.lastError().text();
    }
    else
    {
        qDebug()<<"连接数据库成功";
    }
    QSqlQuery query(dbconn);

    //循环识别
    for(int i = 0;i<fileInfo->count(); i++)
    {
        Mat srcMat;//原图地址
        Mat grayMat;//灰度图像
        Mat medianMat;//中值滤波
        Mat binaryMat;//二值图
        Mat resultMat;//结果图像
        vector<vector<Point>> contours;
        srcMat = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);
        //cv::resize(srcMat, srcMat, Size(), 0.5, 0.5);
        grayMat = cv::imread(fileInfo->at(i).filePath().toStdString(), IMREAD_GRAYSCALE);
        medianBlur(grayMat, medianMat, 5);
        threshold(medianMat, binaryMat, 200, 255, THRESH_BINARY);
        findContours(binaryMat, contours, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, cvPoint(0, 0));
        getSizeContours2(contours);
        resultMat = srcMat.clone();
        drawContours(resultMat, contours, -1, Scalar(0,0,255), 1);   // -1 表示所有轮廓
        //获取当前图片名称，采用变量photo_name存储
        string photoname[20];
        string photo_name;
        int count = split(fileInfo->at(i).filePath().toStdString(), photoname, '/');
        for(int j=0;j<count;j++)
        {
            if(!j < count)
            photo_name = photoname[j];
            else
            {
                continue;
            }
        }
        char a[300];
        sprintf_s(a, "./result/light_result/%s", photo_name.c_str());

        if (contours.size() == 0) //无渗水点
        {
            //cout << "no seepage" << endl;
            QString qs1 = QString(R"(REPLACE INTO visible_light_photo(PhotoId,PhotoPath,IsSeepage,PhotoName) VALUES('%1','%2',%3,'%4');)").arg(i).arg(fileInfo->at(i).filePath()).arg("False").arg(fileInfo->at(i).fileName());
            if(query.exec(qs1))
            {
                //cout << "success" << endl;
                continue;
            }
            else
            {
                qDebug()<<query.lastError().text();//输出错误信息
            }
        }
        else  //有渗水点
        {
            query.exec(QString(R"(REPLACE INTO visible_light_photo(PhotoId,PhotoPath,IsSeepage,PhotoName) VALUES('%1','%2',%3,'%4');)").arg(i).arg(fileInfo->at(i).filePath()).arg("True").arg(fileInfo->at(i).fileName()));
        }
//        //qDebug()<<fileInfo->at(i).filePath();
//        //qDebug()<<fileInfo->at(i).fileName();
//        Mat srcMat;//原图地址
//        Mat srcMat2;//原图地址
//        Mat labelMat, statsMat, centrMat;//连通域函数需要
//        Mat binaryMat;//二值图
//        Mat binaryMat2;//二值图
//        Mat resultMat;
//        Mat resultMat2;
//        srcMat = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);
//        cv::resize(srcMat, srcMat, Size(), 0.5, 0.5);
//        //inRange(srcMat, Scalar(100, 100, 100), Scalar(180, 180, 180), binaryMat);//inRange函数二值化【湖水】
//        //Showlargest(srcMat, binaryMat, resultMat, 4);
//        //Showright(resultMat, -5, 300); //225为0.25倍情况下的像素点【这个水体连通域往右边的距离，理论上越大越好，不过太大害怕有误差，所以精确值偏大一点为最佳
//        srcMat.copyTo(resultMat, resultMat);//得到大坝的区域
//        //Duiqi(resultMat, -10, 300);
//        inRange(resultMat, Scalar(180, 170, 170), Scalar(235, 220, 220), binaryMat);//inRange函数二值化【公路】
//        //Showlargest(resultMat, binaryMat, resultMat, 4);
//        //Showleft(resultMat, 30, 130);
//        //Duiqi(resultMat, 95, 100);
//        //srcMat.copyTo(resultMat, resultMat);//得到大坝的区域
//        srcMat = resultMat.clone();
//        char a[300];
//        sprintf_s(a, "./result/light_result/%d.JPG", i);
//        //imwrite(a, resultMat);
//        srcMat2 = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);
//        cv::resize(srcMat2, srcMat2, Size(), 0.25, 0.25);
//        inRange(srcMat2, Scalar(100, 100, 100), Scalar(180, 180, 180), binaryMat2);//inRange函数二值化【湖水】
//        //Showlargest(srcMat2, binaryMat2, resultMat2, 4);
//        //Showright(resultMat2, -5, 300);
//        srcMat2.copyTo(resultMat2, resultMat2);
//        //Duiqi(resultMat2, -10, 300);
//        inRange(resultMat2, Scalar(180, 170, 170), Scalar(235, 220, 220), binaryMat2);
//        //Showlargest(resultMat2, binaryMat2, resultMat2, 4);
//        //Showright(resultMat2, 10, 180);
//        //Duiqi(resultMat2, 170, 100);
//        //srcMat2.copyTo(resultMat2, resultMat2);//得到大坝的区域
//        srcMat2 = resultMat2.clone();
//        inRange(srcMat2, Scalar(140, 140, 140), Scalar(255, 255, 255), binaryMat2);//inRange函数二值化【渗水点】
//        int nComp = cv::connectedComponentsWithStats(binaryMat2, labelMat, statsMat, centrMat, 8, CV_32S);
//        //显示用图像
//        resultMat2 = cv::Mat::zeros(srcMat2.size(), CV_8UC3);
//        std::vector<cv::Vec3b> colors(nComp);
//        //背景使用黑色
//        colors[0] = cv::Vec3b(0, 0, 0);
//        for (int n = 1; n < nComp; n++) {
//            if (statsMat.at<int>(n, 4) > 15)
//            {
//                colors[n] = cv::Vec3b(255, 255, 255);
//            }
//            else
//            {
//                colors[n] = cv::Vec3b(0, 0, 0); //去除面积小的连通域
//            }
//        }
//        for (int y = 0; y < srcMat2.rows; y++)
//        {
//            for (int x = 0; x < srcMat2.cols; x++)
//            {
//                int label = labelMat.at<int>(y, x);
//                CV_Assert(0 <= label && label <= nComp);
//                resultMat2.at<cv::Vec3b>(y, x) = colors[label];
//            }
//        }
//        int YSDNumber = 0;
//        //用红框标记疑似区域
//        for (int i = 1; i < nComp; i++)
//        {
//            if (statsMat.at<int>(i, 4) > 15)
//            {
//                float w = statsMat.at<int>(i, 2);
//                float h = statsMat.at<int>(i, 3);
//                float s = w / h;
//                if (s > 0.4 && s < 2.5)
//                {	//根据矩形的宽高比来排除一些干扰项
//                    Rect bndbox;
//                    //bounding box左上角坐标
//                    bndbox.x = statsMat.at<int>(i, 0);
//                    bndbox.y = statsMat.at<int>(i, 1);
//                    //bouding box的宽和高
//                    bndbox.width = statsMat.at<int>(i, 2);
//                    bndbox.height = statsMat.at<int>(i, 3);
//                    //绘制
//                    rectangle(srcMat2, bndbox, CV_RGB(255, 0, 0), 1, 8, 0);
//                    YSDNumber = YSDNumber + 1;
//                }
//            }
//        }
        process.setValue(i);
        if(process.wasCanceled())
            break;
        imwrite(a, resultMat);
        QImage* img=new QImage;
        img->load(fileInfo->at(i).filePath());
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
        QCoreApplication::processEvents();
    }
    QMessageBox::about(this,"提示","识别成功");
}



//查看可见光图像识别结果
void Recognition::on_showresult_clicked()
{
    flag = 1;
    QDir dir("./result/light_result");
    QStringList nameFilters;
    nameFilters << "*.jpg" << "*.png";
    QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
    //qDebug() << img_files;
    index = 0;
    QString img_path = img_files->at(index).filePath();

    //连接数据库，将识别结果写入数据库
    QSqlDatabase dbconn = QSqlDatabase::addDatabase("QSQLITE");
    dbconn.setDatabaseName("./datebase/fx.db");  //数据库文件
    if(!dbconn.open())
    {
        qDebug()<<dbconn.lastError().text();
    }
    else
    {
        qDebug()<<"连接数据库成功";
    }
    QSqlQuery query(dbconn);
    query.prepare ("SELECT * FROM visible_light_photo");
    query.exec();
    QString result="";
    //数据库中查询检测结果的数据
    while(query.next())
    {
        //qDebug() <<  QString("PhotoId:%1, PhotoPath:%2, IsSeepage:%3").arg(query.value("PhotoId").toString()).arg(query.value("PhotoPath").toString()).arg(query.value("IsSeepage").toString());
        result += query.value("PhotoId").toString()+' ';
        result += query.value("PhotoPath").toString()+' ';
        //result += query.value("IsSeepage").toString()+' ';
        if(query.value("IsSeepage") == 0)
        {
            result += "无渗水;";
        }
        else
        {
            result += "有渗水;";
        }
        qDebug() << result;
    }
    if(img_path.isEmpty())
    {
         return;
    }
    else
    {
        QImage* img=new QImage;
        if(! (img->load(img_path) ) ) //加载图像路径
        {
            QMessageBox::information(this,
                                     tr("打开图像失败"),
                                     tr("打开图像失败!"));
            delete img;
            return;
        }
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
        //label_other_1
        QString img_path_1 = img_files->at(index).filePath();
        QImage image1(img_path_1);
        QImage Image1 = ImageCenter(image1,ui->label_other_1);
        ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
        ui->label_other_1->setAlignment(Qt::AlignCenter);
        //label_other_2
        QString img_path_2 = img_files->at(index+1).filePath();
        QImage image2(img_path_2);
        QImage Image2 = ImageCenter(image2,ui->label_other_2);
        ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
        ui->label_other_2->setAlignment(Qt::AlignCenter);
        //label_other_3
        QString img_path_3 = img_files->at(index+2).filePath();
        QImage image3(img_path_3);
        QImage Image3 = ImageCenter(image3,ui->label_other_3);
        ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
        ui->label_other_3->setAlignment(Qt::AlignCenter);
        //label_other_4
        QString img_path_4 = img_files->at(index+3).filePath();
        QImage image4(img_path_4);
        QImage Image4 = ImageCenter(image4,ui->label_other_4);
        ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
        ui->label_other_4->setAlignment(Qt::AlignCenter);
        //label_other_5
        QString img_path_5 = img_files->at(index+4).filePath();
        QImage image5(img_path_5);
        QImage Image5 = ImageCenter(image5,ui->label_other_5);
        ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
        ui->label_other_5->setAlignment(Qt::AlignCenter);
        QString str = "当前图片编号:";
        QString str_index = QString::number(index+1);
        str.append(str_index);
        QString str_count = QString::number(img_files->count());
        QString str1 = "    图片总数:";
        str.append(str1);
        str.append(str_count);
        QString str2 = "    图片名:";
        str.append(str2);
        str.append(img_files->at(index).fileName());
        ui->label->setText(str);
    }
}

//下一张图片
void Recognition::on_next_button_clicked()
{
    if(flag==1)
    {
        QDir dir("./result/light_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        if(index<img_files->count()-5)
        {
            index=qAbs(index+1);
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                QString str2 = "    图片名:";
                str.append(str2);
                str.append(img_files->at(index).fileName());
                ui->label->setText(str);
            }
        }else
        {
            index = qAbs(img_files->count()-5);
        }
    }
    else if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        if(index<img_files->count()-5)
        {
            index=qAbs(index+1);
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                QString str2 = "    图片名:";
                str.append(str2);
                str.append(img_files->at(index).fileName());
                ui->label->setText(str);
            }
        }else
        {
            index = qAbs(img_files->count()-5);
        }
    }
    else if(flag == 3)
    {
        QDir dir("./result/red_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        if(index<img_files->count()-5)
        {
            index=qAbs(index+1);
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }
                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                QString str2 = "    图片名:";
                str.append(str2);
                str.append(img_files->at(index).fileName());
                ui->label->setText(str);
            }
        }else
        {
            index = qAbs(img_files->count()-5);
        }
    }
}

//上一张图片
void Recognition::on_last_button_clicked()
{
    if(flag==1)
    {
        if(index>=1)
        {
            index=(index-1);
            QDir dir("./result/light_result");
            QStringList nameFilters;
            nameFilters << "*.jpg" << "*.png";
            QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }

                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                QString str2 = "    图片名:";
                str.append(str2);
                str.append(img_files->at(index).fileName());
                ui->label->setText(str);
            }
        }else
        {
            index = 0;
        }
    }
    else if(flag==2)
    {
        if(index>=1)
        {
            index=(index-1);
            QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
            QStringList nameFilters;
            nameFilters << "*.jpg" << "*.png";
            QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }

                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                QString str2 = "    图片名:";
                str.append(str2);
                str.append(img_files->at(index).fileName());
                ui->label->setText(str);
            }
        }else
        {
            index = 0;
        }
    }
    else if(flag == 3)
    {
        if(index>=1)
        {
            index=(index-1);
            QDir dir("./result/red_result");
            QStringList nameFilters;
            nameFilters << "*.jpg" << "*.png";
            QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
            QString img_path = img_files->at(index).filePath();
            img_path_1 = img_files->at(index).filePath();
            QImage image1(img_path_1);
            QImage Image1 = ImageCenter(image1,ui->label_other_1);
            ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
            ui->label_other_1->setAlignment(Qt::AlignCenter);
            img_path_2 = img_files->at(index+1).filePath();
            QImage image2(img_path_2);
            QImage Image2 = ImageCenter(image2,ui->label_other_2);
            ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
            ui->label_other_2->setAlignment(Qt::AlignCenter);
            img_path_3 = img_files->at(index+2).filePath();
            QImage image3(img_path_3);
            QImage Image3 = ImageCenter(image3,ui->label_other_3);
            ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
            ui->label_other_3->setAlignment(Qt::AlignCenter);
            img_path_4 = img_files->at(index+3).filePath();
            QImage image4(img_path_4);
            QImage Image4 = ImageCenter(image4,ui->label_other_4);
            ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
            ui->label_other_4->setAlignment(Qt::AlignCenter);
            img_path_5 = img_files->at(index+4).filePath();
            QImage image5(img_path_5);
            QImage Image5 = ImageCenter(image5,ui->label_other_5);
            ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
            ui->label_other_5->setAlignment(Qt::AlignCenter);
            if(img_path.isEmpty())
            {
                 return;
            }
            else
            {
                QImage* img=new QImage;
                if(! (img->load(img_path) ) ) //加载图像路径
                {
                    QMessageBox::information(this,
                                             tr("打开图像失败"),
                                             tr("打开图像失败!"));
                    delete img;
                    return;
                }

                ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                    ui->show->height(),
                                                                    Qt::KeepAspectRatio,        //保持长宽比例
                                                                    Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
                ui->show->setAlignment(Qt::AlignCenter);
                QString str = "当前图片编号:";
                QString str_index = QString::number(index+1);
                str.append(str_index);
                QString str_count = QString::number(img_files->count());
                QString str1 = "    图片总数:";
                str.append(str1);
                str.append(str_count);
                QString str2 = "    图片名:";
                str.append(str2);
                str.append(img_files->at(index).fileName());
                ui->label->setText(str);
            }
        }else
        {
            index = 0;
        }
    }
}


//由于Qlabel不支持click事件，设置一个透明的button在Qlabel中，点击button就等同于点击Qlabel，以下代码是透明butoon的事件。
void Recognition::on_label_other_1_button_clicked()
{
    if(flag==1)
    {
        QDir dir("./result/light_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+1);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+1).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+1);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+1).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag == 3)
    {
        QDir dir("./result/red_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+1);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+1).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void Recognition::on_label_other_2_button_clicked()
{
    if(flag==1)
    {
        QDir dir("./result/light_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+1).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+2);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+2).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+1).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+2);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+2).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag == 3)
    {
        QDir dir("./result/red_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+1).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+2);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+2).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void Recognition::on_label_other_3_button_clicked()
{
    if(flag==1)
    {
        QDir dir("./result/light_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+2).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+3);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+3).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+2).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+3);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+3).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag == 3)
    {
        QDir dir("./result/red_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+2).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+3);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+3).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void Recognition::on_label_other_4_button_clicked()
{
    if(flag==1)
    {
        QDir dir("./result/light_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+3).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+4);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+4).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+3).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+4);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+4).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
    else if(flag == 3)
    {
        QDir dir("./result/red_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+3).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+4);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+4).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
    }
}

void Recognition::on_label_other_5_button_clicked()
{
    if(flag==1)
    {
        QDir dir("./result/light_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+4).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+5);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+5).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
    }
    else if(flag==2)
    {
        QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+4).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+5);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+5).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
    }
    else if(flag == 3)
    {
        QDir dir("./result/red_result");
        QStringList nameFilters;
        nameFilters << "*.jpg" << "*.png";
        QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
        QString img_path = img_files->at(index+4).filePath();
        if(img_path.isEmpty())
        {
             return;
        }
        else
        {
            QImage* img=new QImage;
            if(! (img->load(img_path) ) ) //加载图像路径
            {
                QMessageBox::information(this,
                                         tr("打开图像失败"),
                                         tr("打开图像失败!"));
                delete img;
                return;
            }

            ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                                ui->show->height(),
                                                                Qt::KeepAspectRatio,        //保持长宽比例
                                                                Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
            ui->show->setAlignment(Qt::AlignCenter);
            QString str = "当前图片编号:";
            QString str_index = QString::number(index+5);
            str.append(str_index);
            QString str_count = QString::number(img_files->count());
            QString str1 = "    图片总数:";
            str.append(str1);
            str.append(str_count);
            QString str2 = "    图片名:";
            str.append(str2);
            str.append(img_files->at(index+5).fileName());
            ui->label->setText(str);
        }
        ui->label_other_1->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_2->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_3->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_4->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(0, 0, 0);");
        ui->label_other_5->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 0, 0);");
    }
}



//塌陷识别
//void Recognition::on_collapse_identification_clicked()
//{
//    //创建输出结果文件夹
//    QString dir_str = "C:/Users/a1124/Desktop/Desktop/result1";
//    QDir file_dir;
//    if (!file_dir.exists(dir_str))
//    {
//        bool res = file_dir.mkpath(dir_str);
//        qDebug() << "新建目录是否成功" << res;
//    }
//    qDebug() << "文件夹已存在";
//    //选择待识别的图片
//    QString filename = QFileDialog::getExistingDirectory();
//    QDir *dir=new QDir(filename);
//    QStringList filter;
//    filter << "*.jpg" << "*.png";
//    QList<QFileInfo> *fileInfo=new QList<QFileInfo>(dir->entryInfoList(filter));
//    QProgressDialog process(this);
//    QString str = "图片总数:";
//    QString filecount = QString::number(fileInfo->count());
//    str.append(filecount);
//    process.setWindowTitle(str);
//    process.setFixedWidth(300);
//    process.setLabelText(tr("正在识别..."));
//    process.setRange(0,fileInfo->count());
//    process.setModal(true);
//    process.setCancelButtonText(tr("取消"));
//    //循环识别
//    for(int i = 0;i<fileInfo->count(); i++)
//    {
//        Mat srcMat;//原图地址
//        Mat srcMat2;//原图地址
//        Mat labelMat, statsMat, centrMat;//连通域函数需要
//        Mat binaryMat;//二值图
//        Mat binaryMat2;//二值图
//        Mat resultMat;
//        Mat resultMat2;
//        srcMat = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);
//        cv::resize(srcMat, srcMat, Size(), 0.25, 0.25);
//        inRange(srcMat, Scalar(100, 100, 100), Scalar(180, 180, 180), binaryMat);//inRange函数二值化【湖水】
//        //Showlargest(srcMat, binaryMat, resultMat, 4);
//        //Showright(resultMat, -5, 300); //225为0.25倍情况下的像素点【这个水体连通域往右边的距离，理论上越大越好，不过太大害怕有误差，所以精确值偏大一点为最佳
//        srcMat.copyTo(resultMat, resultMat);//得到大坝的区域
//        //Duiqi(resultMat, -10, 300);
//        inRange(resultMat, Scalar(180, 170, 170), Scalar(235, 220, 220), binaryMat);//inRange函数二值化【公路】
//        //Showlargest(resultMat, binaryMat, resultMat, 4);
//        //Showleft(resultMat, 30, 130);
//        //Duiqi(resultMat, 95, 100);
//        srcMat.copyTo(resultMat, resultMat);//得到大坝的区域
//        srcMat = resultMat.clone();
//        char a[300];
//        sprintf_s(a, "C:/Users/a1124/Desktop/Desktop/result/%d.JPG", i);
//        inRange(srcMat, Scalar(150, 160, 150), Scalar(255, 255, 255), binaryMat2);//inRange函数二值化【渗水点】
//        int nComp = cv::connectedComponentsWithStats(binaryMat2, labelMat, statsMat, centrMat, 8, CV_32S);
//        //显示用图像
//        resultMat = cv::Mat::zeros(srcMat.size(), CV_8UC3);
//        std::vector<cv::Vec3b> colors(nComp);
//        //背景使用黑色
//        colors[0] = cv::Vec3b(0, 0, 0);
//        for (int n = 1; n < nComp; n++) {
//                if (statsMat.at<int>(n, 4) > 15) {
//                    colors[n] = cv::Vec3b(255, 255, 255);
//                }
//                else {
//                    colors[n] = cv::Vec3b(0, 0, 0); //去除面积小的连通域
//                }
//            }
//            for (int y = 0; y < srcMat.rows; y++) {
//                for (int x = 0; x < srcMat.cols; x++) {
//                    int label = labelMat.at<int>(y, x);
//                    CV_Assert(0 <= label && label <= nComp);
//                    resultMat.at<cv::Vec3b>(y, x) = colors[label];
//                }
//            }
//            int YSDNumber = 0;
//            //用红框标记疑似区域
//            for (int i = 1; i < nComp; i++) {
//                //char num[10];
//                //sprintf_s(num, "%d", i);
//                if (statsMat.at<int>(i, 4) > 15) {
//                    float w = statsMat.at<int>(i, 2);
//                    float h = statsMat.at<int>(i, 3);
//                    float s = w / h;
//                    if (s > 0.4 && s < 2.5) {	//根据矩形的宽高比来排除一些干扰项
//                        Rect bndbox;
//                        //bounding box左上角坐标
//                        bndbox.x = statsMat.at<int>(i, 0);
//                        bndbox.y = statsMat.at<int>(i, 1);
//                        //bouding box的宽和高
//                        bndbox.width = statsMat.at<int>(i, 2);
//                        bndbox.height = statsMat.at<int>(i, 3);
//                        //绘制
//                        rectangle(srcMat, bndbox, CV_RGB(255, 0, 0), 1, 8, 0);
//                        YSDNumber = YSDNumber + 1;
//                }
//            }
//        }
//        process.setValue(i);
//        if(process.wasCanceled())
//            break;
//        imwrite(a, srcMat);
//        QImage* img=new QImage;
//        img->load(fileInfo->at(i).filePath());
//        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
//                                                            ui->show->height(),
//                                                            Qt::KeepAspectRatio,        //保持长宽比例
//                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
//        ui->show->setAlignment(Qt::AlignCenter);
//        QCoreApplication::processEvents();
//    }
//    QMessageBox::about(this,"提示","识别成功");
//}

//查看塌陷
//void Recognition::on_showresult_collapse_clicked()
//{
//    flag = 2;
//    QDir dir("C:/Users/a1124/Desktop/Desktop/result1");
//    QStringList nameFilters;
//    nameFilters << "*.jpg" << "*.png";
//    QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
//    index = 0;
//    QString img_path = img_files->at(index).filePath();
//    if(img_path.isEmpty())
//    {
//         return;
//    }
//    else
//    {
//        QImage* img=new QImage;
//        if(! (img->load(img_path) ) ) //加载图像路径
//        {
//            QMessageBox::information(this,
//                                     tr("打开图像失败"),
//                                     tr("打开图像失败!"));
//            delete img;
//            return;
//        }
//        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
//                                                            ui->show->height(),
//                                                            Qt::KeepAspectRatio,        //保持长宽比例
//                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
//        ui->show->setAlignment(Qt::AlignCenter);
//        //label_other_1
//        QString img_path_1 = img_files->at(index).filePath();
//        QImage image1(img_path_1);
//        QImage Image1 = ImageCenter(image1,ui->label_other_1);
//        ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
//        ui->label_other_1->setAlignment(Qt::AlignCenter);
//        //label_other_2
//        QString img_path_2 = img_files->at(index+1).filePath();
//        QImage image2(img_path_2);
//        QImage Image2 = ImageCenter(image2,ui->label_other_2);
//        ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
//        ui->label_other_2->setAlignment(Qt::AlignCenter);
//        //label_other_3
//        QString img_path_3 = img_files->at(index+2).filePath();
//        QImage image3(img_path_3);
//        QImage Image3 = ImageCenter(image3,ui->label_other_3);
//        ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
//        ui->label_other_3->setAlignment(Qt::AlignCenter);
//        //label_other_4
//        QString img_path_4 = img_files->at(index+3).filePath();
//        QImage image4(img_path_4);
//        QImage Image4 = ImageCenter(image4,ui->label_other_4);
//        ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
//        ui->label_other_4->setAlignment(Qt::AlignCenter);
//        //label_other_5
//        QString img_path_5 = img_files->at(index+4).filePath();
//        QImage image5(img_path_5);
//        QImage Image5 = ImageCenter(image5,ui->label_other_5);
//        ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
//        ui->label_other_5->setAlignment(Qt::AlignCenter);
//        QString str = "当前图片编号:";
//        QString str_index = QString::number(index+1);
//        str.append(str_index);
//        QString str_count = QString::number(img_files->count());
//        QString str1 = "    图片总数:";
//        str.append(str1);
//        str.append(str_count);
//        ui->label->setText(str);
//    }
//}


//红外图像检测渗水点
void Recognition::on_infrared_identification_clicked()
{
    //创建输出结果文件夹
    QString dir_str = "./result/red_result";
    QDir file_dir;
    if (!file_dir.exists(dir_str))
    {
        bool res = file_dir.mkpath(dir_str);
        qDebug() << "新建目录是否成功" << res;
    }
    qDebug() << "文件夹已存在";
    //选择待识别的图片
    QString filename = QFileDialog::getExistingDirectory();
    QDir *dir=new QDir(filename);
    QStringList filter;
    filter << "*.jpg" << "*.png";
    QList<QFileInfo> *fileInfo=new QList<QFileInfo>(dir->entryInfoList(filter));
    QProgressDialog process(this);
    QString str = "图片总数:";
    QString filecount = QString::number(fileInfo->count());
    str.append(filecount);
    process.setWindowTitle(str);
    process.setFixedWidth(300);
    process.setLabelText(tr("正在识别..."));
    process.setRange(0,fileInfo->count());
    process.setModal(true);
    process.setCancelButtonText(tr("取消"));

    //连接数据库，将识别结果写入数据库
    QSqlDatabase dbconn = QSqlDatabase::addDatabase("QSQLITE");
    dbconn.setDatabaseName("./datebase/fx.db");  //数据库文件
    if(!dbconn.open())
    {
        qDebug()<<dbconn.lastError().text();
    }
    else
    {
        qDebug()<<"连接数据库成功";
    }
    QSqlQuery query(dbconn);


    //循环识别
    for(int i = 0;i<fileInfo->count(); i++)
    {
        Mat imgsrc;
        Mat gray;
        Mat result;
        Mat binaryMat;
        Mat labelMat, statsMat, centrMat;
        imgsrc = cv::imread(fileInfo->at(i).filePath().toStdString(), 1);

        //获取当前图片名称，采用变量photo_name存储
        string photoname[20];
        string photo_name;
        int count = split(fileInfo->at(i).filePath().toStdString(), photoname, '/');
        for(int j=0;j<count;j++)
        {
            if(!j < count)
            photo_name = photoname[j];
            else
            {
                continue;
            }
        }
        //cout << photo_name <<endl;

        //threshold(imgsrc, result, 100, 255, CV_THRESH_BINARY);  //红蓝图像
        threshold(imgsrc, result, 65, 255, CV_THRESH_BINARY_INV);  //灰白图像
        cvtColor(result, gray, COLOR_BGR2GRAY);
        inRange(gray, Scalar(0, 0, 0), Scalar(0, 0, 0), binaryMat);

        vector<vector<Point>> contours;
        //CV_CHAIN_APPROX_NONE  获取每个轮廓每个像素点
        findContours(binaryMat, contours, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, cvPoint(0, 0));
        getSizeContours(contours);

        char a[300];
        sprintf_s(a, "./result/red_result//%s", photo_name.c_str()); //c_str()函数返回一个指向正规C字符串的指针, 内容与本string串相同.
        QString qstr_a = QString::fromStdString(a);
        //QString qstr_photoname = QString::fromStdString(photo_name);
        //cout << a << endl;
        //qDebug() << QDateTime::currentDateTime().toString("yyyyMMddhhmmsszzz");

        //cout << contours.size() << endl;
        if (contours.size() == 0) //无渗水点
        {
            //cout << "no seepage" << endl;
            QString qs1 = QString(R"(REPLACE INTO infrared_photo(PhotoId,PhotoPath,IsSeepage,PhotoName) VALUES('%1','%2',%3,'%4');)").arg(i).arg(qstr_a).arg("False").arg(fileInfo->at(i).fileName());
            if(query.exec(qs1))
            {
                //cout << "success" << endl;
                continue;
            }
            else
            {
                qDebug()<<query.lastError().text();//输出错误信息
            }
        }
        else  //有渗水点
        {
            query.exec(QString(R"(REPLACE INTO infrared_photo(PhotoId,PhotoPath,IsSeepage,PhotoName) VALUES('%1','%2',%3,'%4');)").arg(i).arg(qstr_a).arg("True").arg(fileInfo->at(i).fileName()));
        }

        Mat resultMat(imgsrc.size(), CV_8U, Scalar(255));
        drawContours(imgsrc, contours, -1, Scalar(193,255,0), 2);   // -1 表示所有轮廓

        process.setValue(i);
        if(process.wasCanceled())
            break;
        imwrite(a, imgsrc);
        QImage* img=new QImage;
        img->load(fileInfo->at(i).filePath());
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
        QCoreApplication::processEvents();
    }
    QMessageBox::about(this,"提示","识别成功");
}

//查看红外识别结果
void Recognition::on_showresult_infrared_clicked()
{
    flag = 3;
    QDir dir("./result/red_result");
    QStringList nameFilters;
    nameFilters << "*.jpg" << "*.png";
    QList<QFileInfo> *img_files=new QList<QFileInfo>(dir.entryInfoList(nameFilters));
    index = 0;
    QString img_path = img_files->at(index).filePath();
    
    //连接数据库
    QSqlDatabase dbconn = QSqlDatabase::addDatabase("QSQLITE");
    dbconn.setDatabaseName("./datebase/fx.db");  //数据库文件
    if(!dbconn.open())
    {
        qDebug()<<dbconn.lastError().text();
    }
    else
    {
        qDebug()<<"连接数据库成功";
    }
    QSqlQuery query(dbconn);
    query.prepare ("SELECT * FROM infrared_photo");
    query.exec();
    QString result="";
    //数据库中查询检测结果的数据
    while(query.next())
    {
        //qDebug() <<  QString("PhotoId:%1, PhotoPath:%2, IsSeepage:%3").arg(query.value("PhotoId").toString()).arg(query.value("PhotoPath").toString()).arg(query.value("IsSeepage").toString());
        result += query.value("PhotoId").toString()+' ';
        result += query.value("PhotoPath").toString()+' ';
        //result += query.value("IsSeepage").toString()+' ';
        if(query.value("IsSeepage") == 0)
        {
            result += "无渗水;";
        }
        else
        {
            result += "有渗水;";
        }
        qDebug() << result;
    }

    if(img_path.isEmpty())
    {
         return;
    }
    else
    {
        QImage* img=new QImage;
        if(! (img->load(img_path) ) ) //加载图像路径
        {
            QMessageBox::information(this,
                                     tr("打开图像失败"),
                                     tr("打开图像失败!"));
            delete img;
            return;
        }
        ui->show->setPixmap(QPixmap::fromImage(*img).scaled(ui->show->width(),
                                                            ui->show->height(),
                                                            Qt::KeepAspectRatio,        //保持长宽比例
                                                            Qt::SmoothTransformation)); //平滑处理，使图片缩小时不失真
        ui->show->setAlignment(Qt::AlignCenter);
        //label_other_1
        QString img_path_1 = img_files->at(index).filePath();
        QImage image1(img_path_1);
        QImage Image1 = ImageCenter(image1,ui->label_other_1);
        ui->label_other_1->setPixmap(QPixmap::fromImage(Image1));
        ui->label_other_1->setAlignment(Qt::AlignCenter);
        //label_other_2
        QString img_path_2 = img_files->at(index+1).filePath();
        QImage image2(img_path_2);
        QImage Image2 = ImageCenter(image2,ui->label_other_2);
        ui->label_other_2->setPixmap(QPixmap::fromImage(Image2));
        ui->label_other_2->setAlignment(Qt::AlignCenter);
        //label_other_3
        QString img_path_3 = img_files->at(index+2).filePath();
        QImage image3(img_path_3);
        QImage Image3 = ImageCenter(image3,ui->label_other_3);
        ui->label_other_3->setPixmap(QPixmap::fromImage(Image3));
        ui->label_other_3->setAlignment(Qt::AlignCenter);
        //label_other_4
        QString img_path_4 = img_files->at(index+3).filePath();
        QImage image4(img_path_4);
        QImage Image4 = ImageCenter(image4,ui->label_other_4);
        ui->label_other_4->setPixmap(QPixmap::fromImage(Image4));
        ui->label_other_4->setAlignment(Qt::AlignCenter);
        //label_other_5
        QString img_path_5 = img_files->at(index+4).filePath();
        QImage image5(img_path_5);
        QImage Image5 = ImageCenter(image5,ui->label_other_5);
        ui->label_other_5->setPixmap(QPixmap::fromImage(Image5));
        ui->label_other_5->setAlignment(Qt::AlignCenter);

        QString str = "当前图片编号:";
        QString str_index = QString::number(index+1);
        str.append(str_index);
        QString str_count = QString::number(img_files->count());
        QString str1 = "    图片总数:";
        str.append(str1);
        str.append(str_count);
        QString str2 = "    图片名:";
        str.append(str2);
        str.append(img_files->at(index).fileName());
        ui->label->setText(str);
    }
}



