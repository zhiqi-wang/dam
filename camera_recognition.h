#ifndef CAMERA_RECOGNITION_H
#define CAMERA_RECOGNITION_H

#include <QWidget>
#include <QSqlTableModel>
#include <QStringListModel>
#include <QStandardItemModel>
#include <QModelIndex>
#include<iostream>
#include<math.h>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <stack>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QLabel>

using namespace cv;
using namespace std;

namespace Ui {
class camera_recognition;
}

class camera_recognition : public QWidget
{
    Q_OBJECT

public:
    explicit camera_recognition(QWidget *parent = nullptr);
    ~camera_recognition();
    QImage ImageCenter(QImage qimage,QLabel *qLabel);//调整图片比例
    void getSizeContours(vector<vector<Point>>& contours);
    int index =0;//图片index
    QString img_path_1;
    QString img_path_2;
    QString img_path_3;
    QString img_path_4;
    QString img_path_5;
    int flag = 0;

private slots:
    void on_showresult_collapse_clicked();

    void on_last_button_clicked();

    void on_next_button_clicked();

    void on_label_other_1_button_clicked();

    void on_label_other_1_button_2_clicked();

    void on_label_other_1_button_3_clicked();

    void on_label_other_1_button_4_clicked();

    void on_label_other_1_button5_clicked();

    void on_collapse_identification_clicked();

    void on_collapse_identification_night_clicked();

    void on_showresult_collapse_night_clicked();

private:
    Ui::camera_recognition *ui;
};

#endif // CAMERA_RECOGNITION_H
